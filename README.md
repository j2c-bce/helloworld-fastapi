# helloworld-fastapi

_"Hello World" application in Python_ → [Swagger/OpenAPI description](./swagger.json)

This application provides a simple "greeter" api returning a greeting to
the name given in the URL `/greet/<name>`.

Only letters, numbers, punctuation characters and spaces are allowed in a name.
See the [Allowed Characters](./docs/allowed-characters.md) documentation
for details.

If the name contains any other character (eg. a `+`, math category),
the application returns a **BAD REQUEST** (HTTP status code 400)
response with a problem document.

## Environment variables

### HELLOWORLD_INVALID_NAME_URL

By default, the `type` member of the problem document
(returned when the given name contains invalid characters)
contains the initial URL of the specification
(<https://gitlab.com/j2c-bce/helloworld-fastapi/-/blob/main/docs/allowed-characters.md>).

This environment variable can be used to override the default,
should the specification be moved to another URL.

In the docker image, this variable will be set to the location on GitLab pages
(<https://j2c-bce.gitlab.io/helloworld-fastapi/allowed-characters/>).

## Running the application locally on your workstation

Using a [virtual environment](https://docs.python.org/3/tutorial/venv.html)
is strongly recommended.

### Variant 1: from source

#### Install the required packages

```
pip install --upgrade-strategy only-if-needed -r requirements.txt
```

#### Start the application

```
PYTHONPATH=src python -m helloworld_fastapi run
```

The development server listens at local port `8000`.

Access the greeter API using eg. <http://localhost:8000/greet/YOUR_NAME_HERE>

### Variant 2: build a package, install and run it

#### Clean the dist directory

```
rm -f dist/*
```

#### Build the package

```
python -m build
```

This creates a tarball and a wheel (`*.whl`, a binary package) in the `dist` subdirectory.

#### Install the built package

Install the wheel:

```
pip install dist/helloworld_fastapi-*-py3-none-any.whl
```

#### Start the application

_The environment variable PYTHONPATH **must not** be set in this variant._

```
python -m helloworld_fastapi run
```

#### Uninstall the package again after you are done

```
pip uninstall --yes helloworld_fastapi
```

### Variant 3: using the latest docker image

```
docker run -p 8080:8080 registry.gitlab.com/j2c-bce/helloworld-fastapi/helloworld-fastapi:latest
```

The application becomes accessible at local port `8080`.

## Executing unittests locally

### Variant 1: using the source

#### Install the required packages

_[Install the dependencies as described above](#user-content-install-the-required-packages)._

#### Execute the tests

```
PYTHONPATH=src python -m unittest discover
```

### Variant 2: using the installed package

Build and install the package as described
[above](#user-content-variant-2-build-a-package-install-and-run-it).

_The environment variable PYTHONPATH **must not** be set in this variant._

```
python -m unittest discover
```

## Code quality checks

### black

```
black -l 79 --diff tests src/helloworld_fastapi
```

### flake8

```
flake8 --exclude venv
```

### pylint

_Use `--extension-pkg-whitelist=pydantic` to circumvent the
"No name 'BaseModel' in module 'pydantic'" error
(see <https://github.com/PyCQA/pylint/issues/1524#issuecomment-508934142>),_

```
pylint --extension-pkg-whitelist=pydantic tests src/helloworld_fastapi
```
