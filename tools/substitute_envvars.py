#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

substitute_envvars.py

Substitute certain environ variables in the provided file,
except some forbidden keys (to prevent secrets leakage)

This file is part of helloworld-fastapi.

"""


import argparse
import os
import string
import sys


FORBIDDEN_KEYS = (
    "CI_JOB_JWT", "CI_REPOSITORY_URL", "PIP_INDEX_URL", "TOKEN", "PASSWORD"
)


def __get_arguments():
    """Parse command line arguments"""
    argument_parser = argparse.ArgumentParser(
        description="Substitute environment variables in a file"
    )
    argument_parser.add_argument(
        "target_file",
        help="The target file for the substitute operation",
    )
    return argument_parser.parse_args()


def main(arguments):
    """Main script function"""
    substitutions = {}
    for (key, value) in os.environ.items():
        if key in FORBIDDEN_KEYS:
            continue
        #
        substitutions[key] = value
    #
    with open(arguments.target_file, mode="r", encoding="utf-8") as source:
        source_template = string.Template(source.read())
    #
    target_content = source_template.safe_substitute(substitutions)
    with open(arguments.target_file, mode="w", encoding="utf-8") as target:
        target.write(target_content)
    #
    return 0


if __name__ == '__main__':
    sys.exit(main(__get_arguments()))


#
