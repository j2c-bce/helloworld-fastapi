# =============================================================================
# Pipeline configuration for helloworld-fastapi
# =============================================================================


stages:
  - preparations
  - codestyle
  - test
  - build
  - deploy


# Do secrets detection and SAST
include:
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml


# Re-usable script:
# Print python version and install packages from the DEPENDENCIES environment variable
.install-dependencies: &install-dependencies
  - python --version
  - python -m pip install --upgrade pip
  - >-
    if [ -z "${DEPENDENCIES}" ] ; then
      echo "No package dependencies." ;
    else
      echo "Installing packages: ${DEPENDENCIES} ..." ;
      python -m pip install ${DEPENDENCIES} ;
    fi


# Job template: use python 3.9 natively
# Change pip's cache directory to be inside the project directory since we can
# only cache local items, see
# <https://docs.gitlab.com/ee/ci/caching/#cache-python-dependencies>
.python39-native:
  image: 'python:3.9-slim'
  cache:
    paths:
      - .cache/pip
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
    DEPENDENCIES: ""
  before_script:
    - *install-dependencies


# Job template: use python3.9 in a virtualenv called venv
.python39-virtualenv:
  extends: .python39-native
  before_script:
    - python -m venv venv
    - source venv/bin/activate
    - *install-dependencies


# =============================================================================
# Preparations stage:
# - Compare the tag version to the designated package version
# =============================================================================


# In tagged builds, compare the designated package version to the tag version.
# SCM tags must prefix the version with a lowercase "v", see:
# <https://semver.org/spec/v1.0.0.html#tagging-specification-semvertag>
compare tag and package version:
  stage: preparations
  extends: .python39-native
  script:
    - package_version=$(PYTHONPATH=src python -m helloworld_fastapi version)
    - echo "Checking if the package version matches the commit tag..."
    - if [ "x${CI_COMMIT_TAG}" != "xv${package_version}" ] ; then
          echo "Package version ${package_version} does NOT match commit tag ${CI_COMMIT_TAG}!";
          exit 1;
      else
          echo "Package version ${package_version} matches commit tag ${CI_COMMIT_TAG}.";
      fi
  only:
    - tags


# =============================================================================
# Codestyle stage:
# - PEP 8 compliance check using flake8
# - Static code analysis and scoring using pylint
# =============================================================================


# Static code checks using flake8
flake8 code check:
  stage: codestyle
  extends: .python39-native
  variables:
    DEPENDENCIES: "flake8"
  script:
    - flake8 --exclude venv


# Static code checks using pylint.
# In this case, --extension-pkg-whitelist=pydantic is required
# to resolve C extensions in the pydantic module.
pylint code check:
  stage: codestyle
  extends: .python39-virtualenv
  variables:
    DEPENDENCIES: "-r requirements.txt pylint"
  script:
    - pylint --extension-pkg-whitelist=pydantic --reports=y tests src


# =============================================================================
# Test stage:
# - SAST and secrets-detection (via include)
# - Check dependencies using jake with the OSS index
# (see <https://pythonspeed.com/articles/docker-python-security-scan/>)
# - Unit tests using coverage to measure code coverage
# - Unit tests using tox and pytest to provide JUnit compatible results
# =============================================================================


# - now via include -
# # Static security tests of the application itself using bandit, see
# # <https://bandit.readthedocs.io/>
# bandit sast:
#   stage: test
#   extends: .python39-native
#   variables:
#     DEPENDENCIES: "bandit"
#   script:
#     - bandit -r src


# Vulnerabilities check of the dependencies using jake, see
# <https://github.com/sonatype-nexus-community/jake>
jake dependencies scan:
  stage: test
  extends: .python39-virtualenv
  variables:
    DEPENDENCIES: "-r requirements.txt jake"
  script:
    - jake ddt


# Execute unit tests, determine test coverage and provide the report as an XML file.
# The AWK script tools/extract_percentage.awk is used to extract the percentage
# from the output of "coverage report".
test coverage determination:
  stage: test
  extends: .python39-virtualenv
  variables:
    DEPENDENCIES: "-r requirements.txt coverage"
    PYTHONPATH: src
  script:
    - coverage run -m unittest discover
    - coverage report -m --precision=2
    - coverage xml -o coverage.xml
  coverage: '/^TOTAL\s+\d+\s+\d+\s+(\d+\.\d\d)%/'
  artifacts:
    reports:
      cobertura: coverage.xml


# Execute unit tests and provide the report as an XML file
# (which will visualized in the Gitlab GUI)
tox unittests:
  stage: test
  extends: .python39-native
  variables:
    DEPENDENCIES: "tox"
  script:
    - tox
  artifacts:
    reports:
      junit: testreport.xml


# =============================================================================
# Build stage;
# - Build Python package (.tar.gz / .whl)
# =============================================================================


# Build the python package, determine the image version
# (i.e. package version "SNAPSHOT-<branch>" in untagged builds)
# and pass it as environment variable IMAGE_VERSION
# to the following jobs, as documented in
# <https://docs.gitlab.com/ee/ci/variables/#pass-an-environment-variable-to-another-job>
build package:
  stage: build
  extends: .python39-native
  variables:
    DEPENDENCIES: "build"
  script:
    - python -m build
    - image_version=$(PYTHONPATH=src python -m helloworld_fastapi version)
    - if [ -z "${CI_COMMIT_TAG}" ] ; then image_version="SNAPSHOT-${CI_COMMIT_REF_NAME}"; fi
    - echo "IMAGE_VERSION=${image_version}" > build.env
  artifacts:
    name: 'package'
    paths:
      - dist/
    reports:
      dotenv: build.env
  only:
    - tags
    - branches


# =============================================================================
# Deploy stage:
# - Upload package (tagged builds only)
# - Build and upload container image (all builds)
# - Build HTML documentation (triggering upload to pages URL, tagged builds only)
# =============================================================================


# Upload the package using twine
# Here: use Gitlab’s internal registry, see
# <https://docs.gitlab.com/ee/user/packages/pypi_repository/index.html#authenticate-with-a-ci-job-token>
twine upload:
  stage: deploy
  extends: .python39-virtualenv
  variables:
    DEPENDENCIES: "twine"
    TWINE_PASSWORD: ${CI_JOB_TOKEN}
    TWINE_USERNAME: gitlab-ci-token
    TWINE_REPOSITORY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi
  script:
    - twine check --strict dist/*
    - twine upload dist/*
  only:
    - tags


# Build and push the Docker image to the GitLab image registry
# using Kaniko, see
# <https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#use-kaniko-to-build-docker-images>
# and
# <https://docs.gitlab.com/ee/user/packages/container_registry/#image-naming-convention>
build and upload container image:
  stage: deploy
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --force
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}/helloworld-fastapi:${IMAGE_VERSION}"
      --destination "${CI_REGISTRY_IMAGE}/helloworld-fastapi:latest"
      --build-arg "IMAGE_VERSION=${IMAGE_VERSION}"
  only:
    - tags
    - branches


# Generate HTML documentation from markdown in the docs/ subdirectory
# using mkdocs an the rules in mkdocs.yml.
# On gitlab.com, this triggers another jon in stage "deploy"
# that deploys the documentation to $CI_PAGES_URL
pages:
  stage: deploy
  extends: .python39-virtualenv
  variables:
    DEPENDENCIES: "mkdocs"
  script:
    - python tools/substitute_envvars.py mkdocs.yml
    - cat mkdocs.yml
    - mkdocs build
    - mv site public
  artifacts:
    paths:
      - public
  only:
    - tags


#
