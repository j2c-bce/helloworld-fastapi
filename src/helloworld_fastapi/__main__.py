# -*- coding: utf-8 -*-

"""

helloworld_fastapi.__main__

"Hello World" application local server runner module

"""

import argparse
import sys

import helloworld_fastapi


RETURNCODE_OK = 0
RETURNCODE_ERROR = 1


def __get_argument_parser():
    """Parse command line arguments"""
    argument_parser = argparse.ArgumentParser(
        prog="python -m helloworld",
        description="HelloWorld local invocation",
    )
    subparsers = argument_parser.add_subparsers(
        dest="subcommand",
    )
    subparsers.add_parser(
        "version",
        description="Print application version and exit",
        help="Print application version and exit",
    )
    server_parser = subparsers.add_parser(
        "run",
        description="Run a local server",
        help="Run a local server",
    )
    server_parser.add_argument(
        "--host",
        action="store",
        default="127.0.0.1",
        help="The hostname to listen on (default: %(default)s)",
    )
    server_parser.add_argument(
        "--port",
        action="store",
        type=int,
        default=8000,
        help="The port to listen on (default: %(default)s)",
    )
    server_parser.add_argument(
        "--no-debug",
        action="store_false",
        dest="debug",
        help="Set debug mode off (default: debug mode on)",
    )
    return argument_parser


def main():
    """Main program"""
    argument_parser = __get_argument_parser()
    arguments = argument_parser.parse_args()
    if arguments.subcommand == "version":
        print(helloworld_fastapi.__version__)
    elif arguments.subcommand == "run":
        # pylint: disable=import-outside-toplevel
        import uvicorn

        # pylint: enable
        uvicorn.run(
            "helloworld_fastapi.core:app",
            host=arguments.host,
            port=arguments.port,
            reload=True,
            debug=True,
        )
    else:
        print("A subcommand (version or run) is required.\n")
        argument_parser.print_help()
        return RETURNCODE_ERROR
    #
    return RETURNCODE_OK


if __name__ == "__main__":
    sys.exit(main())


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
