# -*- coding: utf-8 -*-

"""

helloworld_fastapi

"Hello World" application

"""

__version__ = "0.8.2"


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
