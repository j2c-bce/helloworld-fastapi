# -*- coding: utf-8 -*-

"""

helloworld_fastapi.validations

"Hello World" application data validation helper module

"""

import re
import unicodedata


ALLOWED_CATEGORIES = (
    "Ll",  # Letter, lowercase
    "Lm",  # Letter, modifier
    "Lt",  # Letter, titlecase
    "Lu",  # Letter, uppercase
    "Lo",  # Letter, other
    "Nd",  # Number, decimal
    "Nl",  # Number, letter
    "No",  # Number, other
    "Pc",  # Punctuation, connector
    "Pd",  # Punctuation, dash
    "Pi",  # Punctuation, initial quote
    "Pf",  # Punctuation, final quote
    "Ps",  # Punctuation, open
    "Pe",  # Punctuation, close
    "Po",  # Punctuation, other
    "Zs",  # Separator, space
)


def get_invalid_characters(name):
    """Return a list of invalid characters in name"""
    invalid_characters = []
    for index, character in enumerate(name):
        category = unicodedata.category(character)
        if category not in ALLOWED_CATEGORIES:
            try:
                codepoint_name = unicodedata.name(character)
            except ValueError:
                character_display = character
            else:
                character_display = f"{character} ({codepoint_name})"
            #
            invalid_characters.append(
                {
                    "character": character_display,
                    "index": index,
                    "unicode_category": category,
                }
            )
        #
    #
    return invalid_characters


def accepts_media_type(accept_header, media_type):
    """Return True if the accept header accepts the given media type"""
    for item in accept_header.split(","):
        re_media_range = item.split(";")[0].strip().replace("*", ".+?")
        if re.match(f"^{re_media_range}$", media_type):
            return True
        #
    #
    return False


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
