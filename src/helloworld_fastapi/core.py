# -*- coding: utf-8 -*-

"""

helloworld_fastapi.core

"Hello World" ASGI application core module

"""

import os
import socket
import sys

from typing import List

from fastapi import FastAPI, HTTPException, Path, Request
from fastapi.responses import JSONResponse

from pydantic import BaseModel

from helloworld_fastapi import validations
from helloworld_fastapi import __version__


HTTP_BAD_REQUEST = 400
HTTP_NOT_ACCEPTABLE = 406
INVALID_NAME_TITLE = "Invalid name supplied"
INVALID_NAME_URL = (
    os.environ.get("HELLOWORLD_INVALID_NAME_URL")
    or "https://gitlab.com/j2c-bce/helloworld-fastapi/"
    "-/blob/main/docs/allowed-characters.md"
)
DEFAULT_CONTENT_TYPE = "application/json"
PROBLEM_CONTENT_TYPE = "application/problem+json"


class RuntimeInformation(BaseModel):

    """Runtime information"""

    application_version: str
    hostname: str
    python_version: str


class Greeting(BaseModel):

    """Greeting document returned after successful execution"""

    greeting: str
    runtime_information: RuntimeInformation


class InvalidCharacter(BaseModel):

    """Represents an invalid character in a string"""

    character: str
    index: int
    unicode_category: str


class InvalidName(BaseModel):

    """Problem document returned when called with name
    that contains invalid characters
    """

    detail: str
    instance: str
    invalid_characters: List[InvalidCharacter]
    status: int = HTTP_BAD_REQUEST
    title: str = INVALID_NAME_TITLE
    type: str = INVALID_NAME_URL


class InvalidNameError(Exception):

    """Raised if an invalid name was provided"""

    def __init__(self, detail, *invalid_characters):
        """Store the keyword arguments"""
        super().__init__()
        self.detail = detail
        self.invalid_characters = list(invalid_characters)


def get_name_greeting(name):
    """Validate name and return a greeting"""
    invalid_characters = validations.get_invalid_characters(name)
    if invalid_characters:
        raise InvalidNameError(
            f"The provided name {name!r} contains invalid characters",
            *invalid_characters,
        )
    #
    return f"Hello {name}!"


app = FastAPI(
    title="HelloWorld Python FastAPI",
    description="HelloWorld in Python using ASGI (FastAPI)",
    version="1.0",
    license_info={
        "name": "MIT",
        "url": "https://choosealicense.com/licenses/mit/",
    },
)


@app.exception_handler(InvalidNameError)
async def invalid_name_handler(request: Request, error: InvalidNameError):
    """Handle an invalid name error:
    Return a problem document containing all available error data
    """
    problem_document = InvalidName(
        instance=request.url.path,
        invalid_characters=[
            InvalidCharacter(**item) for item in error.invalid_characters
        ],
        detail=error.detail,
    )
    return JSONResponse(
        status_code=HTTP_BAD_REQUEST,
        content=problem_document.dict(),
        media_type=PROBLEM_CONTENT_TYPE,
    )


@app.get(
    "/greet/{name}",
    response_model=Greeting,
    responses={
        200: {
            "description": "Successful operation",
            "content": {
                "application/json": {
                    "example": {
                        "greeting": "Hello World!",
                        "runtime_information": {
                            "application_version": "0.0.1",
                            "hostname": "localhost",
                            "python_version": "3.6.8",
                        },
                    },
                },
            },
        },
        400: {
            "model": InvalidName,
            "description": "Invalid name supplied",
            # This renders the media type incorrectly
            # as "application/json" instead of "application/problem+json"
            # in the generated OpenAPI document.
            # A correct implementation would require the
            # fastapi-rfc7807 module.
        },
    },
)
async def greet(
    request: Request,
    name: str = Path(..., description="The name to greet"),
):
    """Greet {name} or return a BAD REQUEST response"""
    if not validations.accepts_media_type(
        request.headers.get("accept"), DEFAULT_CONTENT_TYPE
    ):
        raise HTTPException(HTTP_NOT_ACCEPTABLE)
    #
    return JSONResponse(
        content=dict(
            greeting=get_name_greeting(name),
            runtime_information={
                "application_version": __version__,
                "hostname": socket.gethostname(),
                "python_version": sys.version,
            },
        )
    )


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
