FROM python:3.9-slim

# Install the pre-built application
COPY dist/*.whl .
RUN pip install --upgrade pip && pip install *.whl

ENV HELLOWORLD_INVALID_NAME_URL="https://j2c-bce.gitlab.io/helloworld-fastapi/allowed-characters/"

EXPOSE 8080

ENTRYPOINT ["python", "-m", "uvicorn", "--host", "0.0.0.0", "--port", "8080", "helloworld_fastapi.core:app"]

ARG IMAGE_VERSION=unknown

LABEL org.opencontainers.image.authors="Rainer Schwarzbach"
LABEL description="A »Hello World« application in Python powered by FastAPI"
LABEL version=$IMAGE_VERSION