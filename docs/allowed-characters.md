# Allowed Characters

See <https://unicodebook.readthedocs.io/unicode.html#categories>
regarding the Unicode categories.

## Letters

All letters defined in the Unicode categories starting with `L`.
This should cover all human alphabets.

## Numbers

All numbers defined in the Unicode categories starting with `N`.

## Punctuation

All numbers defined in the Unicode categories starting with `P`.

## Spaces

Spacing characters defined in the Unicode category `Zs`.
