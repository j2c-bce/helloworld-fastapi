# HelloWorld FastAPI

Simple demo application that will greet the name provided in the /greet/{name} URL
when called using the HTTP GET method.

Only Letters, numbers, spaces and punctuation characters are allowed in a name.

See /docs or /redoc for the API specification.