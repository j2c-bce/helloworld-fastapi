# -*- coding: utf-8 -*-

"""

test_core.py

Test the "Hello World" application core module

"""

import os
import socket
import sys
import unittest

from helloworld_fastapi import core
from helloworld_fastapi import validations
from helloworld_fastapi import __version__

from . import commons


HTTP_BAD_REQUEST = 400
HTTP_NOT_ACCEPTABLE = 406

EXPECTED_RUNTIME_INFO = dict(
    application_version=__version__,
    hostname=socket.gethostname(),
    python_version=sys.version,
)

DEFAULT_CONTENT_TYPE = "application/json"
PROBLEM_CONTENT_TYPE = "application/problem+json"


class APITest(commons.HTTPTest):

    """HTTP tests agains the API"""

    def test_api(self):
        """API called with a random valid name"""
        random_name = os.urandom(8).hex()
        self.check_resource(
            f"/greet/{random_name}",
            accept_header=DEFAULT_CONTENT_TYPE,
            expected_content=dict(
                greeting=f"Hello {random_name}!",
                runtime_information=EXPECTED_RUNTIME_INFO,
            ),
            expected_media_type=DEFAULT_CONTENT_TYPE,
        )

    def test_api_invalid_name(self):
        """API called with an invalid name"""
        invalid_name = "invalid \x01 name"
        request_path = f"/greet/{invalid_name}"
        self.check_resource(
            request_path,
            accept_header=DEFAULT_CONTENT_TYPE,
            expected_content=dict(
                detail=f"The provided name {invalid_name!r}"
                " contains invalid characters",
                instance=request_path,
                invalid_characters=validations.get_invalid_characters(
                    invalid_name
                ),
                status=HTTP_BAD_REQUEST,
                title="Invalid name supplied",
                type=core.INVALID_NAME_URL,
            ),
            expected_media_type=PROBLEM_CONTENT_TYPE,
            expected_status=HTTP_BAD_REQUEST,
        )

    def test_api_unacceptable(self):
        """API called with a valid name,
        but missing not matching Accept: header"""
        valid_name = "valid name"
        self.check_resource(
            f"/greet/{valid_name}",
            accept_header="text/html",
            expected_status=HTTP_NOT_ACCEPTABLE,
        )


class FunctionsTest(unittest.TestCase):

    """Functions test"""

    def test_greet(self):
        """greet function called with a random valid name"""
        random_name = os.urandom(8).hex()
        self.assertEqual(
            core.get_name_greeting(random_name),
            f"Hello {random_name}!",
        )

    def test_greet_invalid_name(self):
        """greet function called with an invalid name"""
        invalid_name = "invalid <name>"
        self.assertRaises(
            core.InvalidNameError,
            core.get_name_greeting,
            invalid_name,
        )


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
