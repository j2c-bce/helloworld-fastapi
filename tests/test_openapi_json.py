# -*- coding: utf-8 -*-

"""

test_openapi_json.py

Test the "Hello World" application OpenAPI document

"""

import json

from . import commons


class OpenAPITest(commons.HTTPTest):

    """API Test"""

    def test_openapi_doc(self):
        """Load OpenAPI definition from the server and compare it
        with the expected one stored in swagger.json
        """
        with open("swagger.json", encoding="utf-8") as swaggerfile:
            self.check_resource(
                "/openapi.json",
                expected_content=json.load(swaggerfile),
            )
        #


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
