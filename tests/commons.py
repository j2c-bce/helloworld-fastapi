# -*- coding: utf-8 -*-

"""

commons.py

Common test functionality

"""

import unittest

from fastapi.testclient import TestClient

from helloworld_fastapi import core


HTTP_OK = 200


class HTTPTest(unittest.TestCase):

    """UnitTest: HTTP"""

    # pylint: disable=too-many-arguments
    def check_resource(
        self,
        url,
        accept_header=None,
        expected_content=None,
        expected_media_type=None,
        expected_status=HTTP_OK,
    ):
        """Load a URL resource from the server and
        check the given expected parameters
        """
        response_args = {}
        if accept_header:
            response_args["headers"] = {"Accept": accept_header}
        #
        response = TestClient(core.app).get(url, **response_args)
        #
        with self.subTest(msg="Status code"):
            self.assertEqual(response.status_code, expected_status)
        #
        if expected_media_type:
            with self.subTest(msg="Media type"):
                self.assertEqual(
                    response.headers.get("Content-Type"), expected_media_type
                )
            #
        #
        if expected_content:
            with self.subTest(msg="Document"):
                self.assertEqual(response.json(), expected_content)
            #
        #


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
