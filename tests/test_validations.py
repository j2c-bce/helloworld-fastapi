# -*- coding: utf-8 -*-

"""

test_validations.py

Test the "Hello World" application validations helper module

"""

import unittest

from helloworld_fastapi import validations


class SimpleTest(unittest.TestCase):

    """Simple unit test"""

    def test_invalid_name(self):
        """Validation function called with an invalid name"""
        self.assertEqual(
            validations.get_invalid_characters("invalid \n name <~"),
            [
                dict(character="\n", index=8, unicode_category="Cc"),
                dict(
                    character="< (LESS-THAN SIGN)",
                    index=15,
                    unicode_category="Sm",
                ),
                dict(character="~ (TILDE)", index=16, unicode_category="Sm"),
            ],
        )

    def test_valid_name(self):
        """Validation function called with a valid name"""
        self.assertEqual(validations.get_invalid_characters("valid name"), [])

    def test_accepted_media_type(self):
        """test accepts_media_type() positive answer"""
        for accept_header in (
            "*/*",
            "application/*",
            "application/json",
            "text/html,audio/*;q=0.8,*/*;q=0.0",
        ):
            with self.subTest(f"Media types {accept_header!r}"):
                self.assertTrue(
                    validations.accepts_media_type(
                        accept_header, "application/json"
                    )
                )
            #
        #

    def test_inacceptable_media_type(self):
        """test accepts_media_type() negative answer"""
        for accept_header in (
            "text/*",
            "text/html",
            "text/html,audio/*,audio/flac;q=0.8",
        ):
            with self.subTest(f"Media types {accept_header!r}"):
                self.assertFalse(
                    validations.accepts_media_type(
                        accept_header, "application/json"
                    )
                )
            #
        #


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
